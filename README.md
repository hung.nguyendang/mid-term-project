**********TỰ ĐÁNH GIÁ ĐỒ ÁN**********

MỨC ĐỘ HOÀN THIỆN THEO YÊU CẦU CỦA GIÁO VIÊN: 100%

- Lấy được danh sách phim từ API của "ThemovieDB".

- Có chức năng tìm kiếm phim theo tên phim hoặc theo tên diễn viên. (để thay đổi chế độ tìm kiếm thì người dùng chọn dropdown list ở gần ô tìm kiếm)

- Có thể hiển thị chi tiết phim (gồm: hình ảnh, tiêu đề, năm sản xuất, diễn viên, đạo diễn, tóm tắt, thể loại) khi nhấn vào mỗi phim.

- Xem được thông tin chi tiết diễn viên (gồm: hình ảnh, tên, tiểu sử) khi nhấp vào tên của diễn viên đó trong trang chi tiết phim.

- Hiển thị được danh sách các phim mà diễn viên có tham gia.

- Hiển thị được reviews cho phim.

- Giao diện, bố cục hợp lý.
- Hiệu ứng đơn giản và dễ nhìn.

